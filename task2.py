import csv
import matplotlib.pyplot as plt

class Played:
    
    # Create an empty dictionary to store the number of matches played by each team in each season
    def exec2(self):
        matches_played = {}

        # Read the CSV file
        with open('Unit Testing/mock_matches.csv') as csvfile:
            csvreader = csv.DictReader(csvfile)
            # Iterate through the CSV data
            for row in csvreader:
                team1 = row['team1']
                team2 = row['team2']
                season = row['season']
                # Update the matches_played dictionary for the first team (team1)
                if team1 in matches_played:
                    if season in matches_played[team1]:
                        matches_played[team1][season] += 1
                    else:
                        matches_played[team1][season] = 1
                else:
                    matches_played[team1] = {season: 1}

                # Update the matches_played dictionary for the second team (team2)
                if team2 in matches_played:
                    if season in matches_played[team2]:
                        matches_played[team2][season] += 1
                    else:
                        matches_played[team2][season] = 1
                else:
                    matches_played[team2] = {season: 1}
        return matches_played
    
    def plot(self, matches_played):
        # Extract the list of team names and sorted seasons
        teams = list(matches_played.keys())
        seasons = [str(season) for season in range(2008, 2018)]

        # Create a figure and axis for the plot
        fig, ax = plt.subplots(figsize=(15, 3))

        # Initialize the 'bottom' list to keep track of the bottom position for each team's bars
        bottom = [0] * len(teams)

        # Iterate through seasons to create a grouped bar chart
        for season in seasons:
            # Get the number of games played by each team for the current season
            games_played = [matches_played[team].get(season, 0) for team in teams]

            # Create a bar chart for the current season and update the 'bottom' positions
            ax.bar(teams, games_played, label=season, bottom=bottom)
            bottom = [sum(x) for x in zip(bottom, games_played)]

        # Set labels, title, and rotate the team names for better visibility
        ax.set_ylabel('Number of Games Played')
        ax.set_xlabel('Teams')
        ax.set_title('Number of Matches Played by Each IPL Team by Season')
        plt.xticks(rotation=30)

        # Add a legend to distinguish seasons and adjust its position
        ax.legend(title='Seasons', loc='upper left', bbox_to_anchor=(1, 0))

        # Display the plot
        plt.show()
            
obj=Played()
matches_played=obj.exec2()
print(matches_played)

obj.plot(matches_played)


