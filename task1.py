import csv
import matplotlib.pyplot as plt

class Economy:
    def exec1(self):
        with open('Unit Testing/mock_matches.csv', newline='') as csvfile:
            # Create a DictReader object to read the CSV file as a dictionary
            csv_reader = csv.DictReader(csvfile)
            
            number_of_matches = {}
            # Create an empty dictionary to store the number of matches per season
            
            for row in csv_reader:
                # Iterate through each row in the CSV file
                if row['season'] not in number_of_matches:
                    # If the season is not in the dictionary, add it with a count of 1
                    number_of_matches[row['season']] = 1
                else:
                    # If the season is already in the dictionary, increment the count
                    number_of_matches[row['season']] += 1
        return number_of_matches
    
    def plot(self, matches_played):
        # Create a bar chart with seasons on the x-axis and match counts on the y-axis
        plt.bar(sorted(number_of_matches.keys()), sorted(number_of_matches.values()))
        plt.show()
    
obj=Economy()
number_of_matches=obj.exec1()
print(number_of_matches)

obj.plot(number_of_matches)
