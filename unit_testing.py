import unittest
from task1 import Economy
from task2 import Played
from task3 import ExtraRuns
from task4 import Economic

class TestCalculations(unittest.TestCase):

    def test_Economy(self):
        obj=Economy()
        self.assertEqual(obj.exec1(),{'2008': 3, '2017': 1, '2015': 3, '2009': 1, '2010': 3, '2011': 2, '2016': 3, '2012': 1, '2013': 1}, 'The output is wrong.')

    def test_Played(self):
        obj=Played()
        self.assertEqual(obj.exec2(),{'Kolkata Knight Riders': {'2008': 1, '2017': 1, '2010': 1, '2011': 1, '2015': 1, '2013': 1}, 'Royal Challengers Bangalore': {'2008': 1, '2017': 1, '2010': 1, '2015': 1, '2016': 1}, 'Chennai Super Kings': {'2008': 1, '2015': 2, '2010': 1, '2011': 1, '2016': 1}, 'Kings XI Punjab': {'2008': 1, '2009': 1, '2012': 1}, 'Rajasthan Royals': {'2008': 1, '2015': 1, '2011': 1}, 'Delhi Daredevils': {'2008': 1, '2009': 1, '2010': 1, '2013': 1, '2016': 1}, 'Deccan Chargers': {'2010': 1, '2011': 1, '2015': 1}, 'Mumbai Indians': {'2010': 1, '2016': 2}, 'Pune Warriors': {'2012': 1}, 'Sunrisers Hyderabad': {'2016': 1}}, 'The output is wrong.')

    def test_ExtraRuns(self):
        obj=ExtraRuns()
        self.assertEqual(obj.exec3(),{'Kings XI Punjab': 9, 'Mumbai Indians': 20, 'Delhi Daredevils': 5, 'Gujarat Lions': 23, 'Chennai Super Kings': 19, 'Sunrisers Hyderabad': 16}, 'The output is wrong.')

    def test_Economic(self):
        obj=Economic()
        self.assertEqual(obj.exec4(),{'TA Boult': 15, 'CR Brathwaite': 6, 'AD Mathews': 6, 'Kuldeep Yadav': 6, 'SP Narine': 1, 'MM Sharma': 6, 'Sandeep Sharma': 6, 'VR Aaron': 6}, 'The output is wrong.')

if __name__ == '__main__':
    unittest.main()