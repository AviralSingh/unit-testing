import csv
import matplotlib.pyplot as plt

class Economic:
    
    def exec4(self):
        # Open the CSV file
        with open('Unit Testing/mock_deliveries.csv', newline='') as csvfile:
            # Create a CSV reader
            dataset = csv.DictReader(csvfile)

            # Initialize dictionaries to store data
            
            bowlers_overs = {}  # To store the number of overs bowled
            bowlers = {}  # To store runs conceded

            # Iterate through each row in the dataset
            for row in dataset:
                # Check if the match is within the specified range
                if int(row['match_id']) in [5, 12, 14]:
                    # Check if it's not a super over
                    if row['bowler'] not in bowlers and row['is_super_over'] != '1':
                        # Initialize the bowler's data if not present
                        bowlers[row['bowler']] = (
                            int(row['total_runs'])
                            - int(row['legbye_runs'])
                            - int(row['bye_runs'])
                        )
                        bowlers_overs[row['bowler']] = 1
                    else:
                        # Update the bowler's data if already present
                        bowlers[row['bowler']] += (
                            int(row['total_runs'])
                            - int(row['legbye_runs'])
                            - int(row['bye_runs'])
                        )
                        bowlers_overs[row['bowler']] += 1
        return [bowlers_overs,bowlers]
    
    def plot(self, bowlers, bowlers_overs):
        # Calculate the number of overs bowled for each bowler
        for i in bowlers_overs:
            bowlers_overs[i] = bowlers_overs[i] / 6

            # Calculate the average runs conceded per over for each bowler
            bowlers[i] = bowlers[i] / bowlers_overs[i]

        # Sort the bowlers by their average runs conceded
        bowlers = dict(sorted(bowlers.items(), key=lambda items: items[1]))

        # Plot the top 10 bowlers with the lowest average runs conceded
        plt.plot(list(bowlers.values())[:10], list(bowlers.keys())[:10], '-or')
        plt.show()
obj=Economic()
bowlers_overs=obj.exec4()
print(bowlers_overs[0])
obj.plot(bowlers_overs[1],bowlers_overs[0])

