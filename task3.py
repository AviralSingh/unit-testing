import csv
# Import the CSV module for reading CSV files
import matplotlib.pyplot as plt
# Import Matplotlib for creating plots

class ExtraRuns:
    def exec3(self):
        with open('Unit Testing/mock_deliveries.csv', newline='') as csvfile:
            # Open the CSV file in a context manager
            csv_reader = csv.DictReader(csvfile)
            # Create a DictReader object to read the CSV file as a dictionary

            extra_runs_per_team = {}
            # Create an empty dictionary to store extra runs per team

            for row in csv_reader:
                # Iterate through each row in the CSV file

                if int(row['match_id']) in [13, 17, 18]:
                    # Check if the match_id is within the specified range (576 to 637)

                    if row['bowling_team'] not in extra_runs_per_team:
                        # If the bowling team is not in the dictionary, add it with the extra runs
                        extra_runs_per_team[row['bowling_team']] = int(row['extra_runs'])
                    else:
                        # If the bowling team is already in the dictionary, add the extra runs to the existing total
                        extra_runs_per_team[row['bowling_team']] += int(row['extra_runs'])
        return extra_runs_per_team
    def plot(self, extra_runs_per_team):
        # Create a plot with team names on the x-axis and extra runs on the y-axis
        plt.plot(extra_runs_per_team.keys(), extra_runs_per_team.values(), '-or')

        # Show the plot
        plt.show()
    
obj=ExtraRuns()
extra_runs_per_team=obj.exec3()
print(extra_runs_per_team)

obj.plot(extra_runs_per_team)
